﻿#from input csv which contains intended labels, generate another csv with pre-defined guids.
$inputcsvpath = "C:\Hemant\tmp\trmcolcsv.csv";
$interimcsvpath = "C:\Hemant\tmp\trmcolcsv2.csv";
$finalcsvpath = "C:\Hemant\tmp\trmcolcsv3.csv";
Import-Csv -Path $inputcsvpath | Select-Object *, @{Name = "guid"; Expression = { New-Guid } } | Export-Csv $interimcsvpath -NoTypeInformation;

#connect to the site
$pwrd = "removed";
$usrnm = "sitecollectionadmin@self30042.onmicrosoft.com"; 
$roositeurl = "https://self30042.sharepoint.com/sites/testhemant1";
$PasswordAsSecure = ConvertTo-SecureString $pwrd -AsPlainText -Force;
$Credentials = New-Object System.Management.Automation.PSCredential ($usrnm , $PasswordAsSecure);  
Connect-PnPOnline -Url $roositeurl -Credentials $Credentials; #-UseWebLogin if it doesn't work

$CurrentSite = Get-PnPSite;
Write-Host $(Get-PnPProperty -ClientObject $CurrentSite -Property Url);
$txnmsession = Get-PnPTaxonomySession; #taxonomy session
$TermStore = $txnmsession.GetDefaultSiteCollectionTermStore();
$SiteCollectionTermGroup = $TermStore.GetSiteCollectionGroup($CurrentSite, $true); # second parameter is createIfMissing
$sitecollGroupName = Get-PnPProperty -ClientObject $SiteCollectionTermGroup -Property Name;
Write-Host "Site Collection Level Group is:" $sitecollGroupName;
if ($sitecollGroupName) {
    $trmsetname = "hk1_hk2_hk3"; #termset name
    $trmsetid = "c9e4867c-397d-4c8a-808d-aa3be723cbfb";
    New-PnPTermSet -Name $trmsetname -TermGroup $sitecollGroupName -Id $trmsetid -IsOpenForTermCreation;
    $termset = Get-PnPTermSet -Identity $trmsetname -TermGroup $sitecollGroupName;
    If ($termset) {

        Import-Csv -Path $interimcsvpath | ForEach-Object {
            #New-PnPTerm -TermSet $TermSetName -TermGroup $IntranetTermGroupName   -Name $_.termlbl; if id is not passed autogenrated by SharePoint
            New-PnPTerm -TermSet $trmsetname -TermGroup $sitecollGroupName   -Name $_.termlbl -Id $_.guid;
        }

        Get-PnPTerm -TermSet $trmsetname -TermGroup $sitecollGroupName | Select-Object Name, Id | Export-Csv $finalcsvpath -NoTypeInformation;

    }


}
Disconnect-PnPOnline